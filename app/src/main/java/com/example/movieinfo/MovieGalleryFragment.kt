package com.example.movieinfo

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.picsum.picsum.Item
import com.squareup.picasso.Picasso

private const val TAG = "MovieGalleryFragment"
// a list with movies
class MovieGalleryFragment : Fragment() {

    private lateinit var photoRecyclerView: RecyclerView
    private lateinit var movieGalleryViewModel: MovieGalleryViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        movieGalleryViewModel = ViewModelProvider(requireActivity())
            .get(MovieGalleryViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_gallery, container, false)
        photoRecyclerView = view.findViewById(R.id.recycler_view)
        photoRecyclerView.layoutManager = GridLayoutManager(context, 1)
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        movieGalleryViewModel.movieLiveData.observe(
            viewLifecycleOwner,
            { movieItems ->
                Log.d(TAG, "new items from ViewModel $movieItems")
                photoRecyclerView.adapter = PhotoAdapter(movieItems)

            })
    }

    private inner class PhotoAdapter(private val galleryItems: List<Item>) :
        RecyclerView.Adapter<ViewHolder>() {

        var lastGaleryItemsSize = 0

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): ViewHolder {
            val view = layoutInflater.inflate(R.layout.gallary_item,
                parent, false) as View
            return ViewHolder(view)
        }

        override fun getItemCount(): Int = galleryItems.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val movieItem = galleryItems[position]
            holder.title.text = movieItem.display_title
            holder.description.text = movieItem.summary_short

            Picasso.get().load(movieItem.multimedia.src)
                .placeholder(R.drawable.empty)
                .error(R.drawable.empty)
                .into(holder.image)

        }
    }

    companion object {
        fun newInstance() = MovieGalleryFragment()
    }
}