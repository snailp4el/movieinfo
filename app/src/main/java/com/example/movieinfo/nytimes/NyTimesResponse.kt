package com.example.picsum.picsum

import com.google.gson.annotations.SerializedName
//for retrofit, not used outside of this package
class NyTimesResponse() {

    @SerializedName("results")
    lateinit var items: List<Item>

}