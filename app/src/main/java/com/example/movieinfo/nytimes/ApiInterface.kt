package com.example.picsum.picsum

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

/*
api id:  e8060691-ad6d-4a85-8132-16ba40e6af0b
api key: uArHrfHqu05jhfpolcnloGOO0hMkw6xA
secret:  dAgF4k6j343a1anY
example:
https://api.nytimes.com/svc/movies/v2/reviews/all.json?r&api-key=uArHrfHqu05jhfpolcnloGOO0hMkw6xA
*/

private const val apiKey = "uArHrfHqu05jhfpolcnloGOO0hMkw6xA"
//retrofit interface
interface ApiInterface {

    @GET("svc/movies/v2/reviews/all.json?r&api-" +
            "key=$apiKey")
    fun getItems() : Call<NyTimesResponse>


    companion object {
        var BASE_URL = "https://api.nytimes.com/"
        fun create() : ApiInterface {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(ApiInterface::class.java)

        }
    }

}