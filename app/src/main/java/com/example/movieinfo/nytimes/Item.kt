package com.example.picsum.picsum
//a POJO with film info
class Item(var display_title: String = "",
           var multimedia: MultimediaResponse,
           var summary_short: String = "")