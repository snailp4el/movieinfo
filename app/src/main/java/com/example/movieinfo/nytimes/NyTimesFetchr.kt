package com.example.movieinfo.nytimes

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.picsum.picsum.ApiInterface
import com.example.picsum.picsum.Item
import com.example.picsum.picsum.NyTimesResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

private const val TAG = "NyTimesFetchr"

//get Items from api.nytimes.com
class NyTimesFetchr {

    private val apiInterface: ApiInterface

    init {
        apiInterface = ApiInterface.create()
    }

    fun fetchItems(responseLiveData: MutableLiveData<MutableList<Item>>) {

        val request: Call<NyTimesResponse> = apiInterface.getItems()

        request.enqueue(object : Callback<NyTimesResponse> {

            override fun onFailure(call: Call<NyTimesResponse>, t: Throwable) {
                Log.e(TAG, "Failed to fetch", t)
                responseLiveData.value = mutableListOf()
            }

            override fun onResponse(
                call: Call<NyTimesResponse>,
                response: Response<NyTimesResponse>
            ) {
                Log.d(TAG, "Response received")
                var items: List<Item> = response.body()?.items ?: mutableListOf()

                items = items.filterNot {
                    it.multimedia.src.isBlank()
                    it.display_title.isBlank()
                }

                responseLiveData.value = items.toMutableList()

            }
        })
    }

}