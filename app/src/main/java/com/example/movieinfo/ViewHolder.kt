package com.example.movieinfo

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

//used by MovieGalleryFragment
class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val title: TextView
    val image: ImageView
    val description:TextView

    init {
        title = view.findViewById(R.id.title)
        image = view.findViewById(R.id.image)
        description = view.findViewById(R.id.description)
    }
}