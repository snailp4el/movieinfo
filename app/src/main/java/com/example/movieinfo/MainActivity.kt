package com.example.movieinfo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val isFragmentContainerEmpty = savedInstanceState == null
        if (isFragmentContainerEmpty) {
            launchSplashScreen()
        }

    }

    fun launchMovieGalleryFragment(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, MovieGalleryFragment.newInstance())
            .commit()
    }

    fun launchSplashScreen(){
        supportFragmentManager
            .beginTransaction()
            .add(R.id.fragmentContainer, SplashScreen.newInstance())
            .commit()
    }

}
