package com.example.movieinfo

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar

private const val TAG = "SplashScreen"

class SplashScreen:Fragment() {

    private lateinit var movieGalleryViewModel: MovieGalleryViewModel
    lateinit var snackbar : Snackbar


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_splash_screen, container, false)
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        snackbar = Snackbar.make(view, R.string.cant_load, Snackbar.LENGTH_INDEFINITE)

        movieGalleryViewModel = ViewModelProvider(requireActivity())
            .get(MovieGalleryViewModel::class.java)

        movieGalleryViewModel.movieLiveData.observe(
            viewLifecycleOwner,
            { galleryItems ->
                Log.d(TAG, "new items from ViewModel $galleryItems")

                //will try to load until success
                if(galleryItems.size != 0){
                    snackbar.dismiss()
                    val a = activity as MainActivity
                    a.launchMovieGalleryFragment()
                }else{
                    movieGalleryViewModel.fetchAgain()
                    snackbar.show()
                }
            })

    }

    companion object {
        fun newInstance() = SplashScreen()
    }

}