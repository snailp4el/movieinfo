package com.example.movieinfo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.movieinfo.nytimes.NyTimesFetchr
import com.example.picsum.picsum.Item

//this class used by MovieGallery fragment and SplashScreen
class MovieGalleryViewModel: ViewModel() {

    var movieLiveData: MutableLiveData<MutableList<Item>> = MutableLiveData()
    val nyTimesFetchr = NyTimesFetchr()

    init {
        nyTimesFetchr.fetchItems(movieLiveData)
    }

    fun fetchAgain(){
        nyTimesFetchr.fetchItems(movieLiveData)
    }

}