This app shows a list of films by using Movie Reviews API.
https://developer.nytimes.com/docs/movie-reviews-api/1/overview

Here you can get apk file:
https://www.dropbox.com/s/k2k5d33tkkrdhcs/app-debug.apk?dl=0

If you looked at my project, please give me feedback.
email: petrushevav@gmail.com
telegram: @snailp4el